
const pactum = require('pactum');
const { Given, When, Then, Before } = require('@cucumber/cucumber');

let spec = pactum.spec();

Before(()=>{
    spec = pactum.spec();
})

Given('I make a GET request to {string}',{ timeout: 2 * 30000 }, async(url)=>{
    // await spec.get(url).expectStatus(200);

//     await spec
//   .withMethod('GET')
//   .withPath(url)
//   .expectStatus(200);

  const reee= await spec.get(url).inspect('email').inspect('website')
//   console.log("GET req done",reee.toString());
    // console.log("SPECURL",spec.get(url));
})

When('I receive a response',{ timeout: 2 * 30000 }, async()=>{
    await spec.toss();
})

Then('I should see {string}',{ timeout: 2 * 30000 },async(body)=>{
    // console.log("RESP",await spec.response());
    await spec.response().should.have.body(body)
})

Then('I get to see (.*)',{ timeout: 2 * 30000 },async(path,value)=>{
    // await spec.response().should.have.body(body)
    await spec.response().should.have.json(path, JSON.parse(value));

})